package com.gerona.nerubiamovieapp.data.ui.movie_list.scroll

interface OnLoadMoreListener {
    fun onLoadMore()
}