package com.gerona.nerubiamovieapp.data.api

class ApiHelper(private val apiService: ApiServices) {

    suspend fun getPopularMovies(page:Int) = apiService.getAllPopularMovies(page)
}