package com.gerona.nerubiamovieapp.data.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gerona.nerubiamovieapp.data.api.ApiHelper
import com.gerona.nerubiamovieapp.data.repository.MainRepository
import com.gerona.nerubiamovieapp.data.ui.movie_list.viewmodel.ListViewModel

class ViewModelFactory(private val apiHelper: ApiHelper) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ListViewModel::class.java)) {
            return ListViewModel(MainRepository(apiHelper)) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}