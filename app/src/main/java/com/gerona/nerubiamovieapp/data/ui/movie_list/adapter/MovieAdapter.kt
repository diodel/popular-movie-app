package com.gerona.nerubiamovieapp.data.ui.movie_list.adapter

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gerona.nerubiamovieapp.R
import com.gerona.nerubiamovieapp.data.model.Movie
import com.gerona.nerubiamovieapp.databinding.ItemMovieListBinding
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MovieAdapter(private val clickListener: (Movie.Results) -> Unit) :
    RecyclerView.Adapter<MovieAdapter.DataViewHolder>() {
    private val movies = ArrayList<Movie.Results>()

    class DataViewHolder(val binding: ItemMovieListBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(movie: Movie.Results, clickListener: (Movie.Results) -> Unit) {
            binding.tvTitle.text = movie.title
            binding.tvDate.text = parseDate(movie.release_date)
            Glide.with(binding.imageViewAvatar.context)
                .load("https://image.tmdb.org/t/p/w500" + movie.poster_path)
                .placeholder(ColorDrawable(Color.GRAY))
                .into(binding.imageViewAvatar)
            itemView.setOnClickListener {
                clickListener(movie)
            }
        }

        private fun parseDate(date: Date): String? {
            val dateFormat = SimpleDateFormat("dd MMM yyyy")
            return dateFormat.format(date)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemMovieListBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.item_movie_list, parent, false)
        return DataViewHolder(binding)
    }

    override fun getItemCount(): Int = movies.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bind(movies[position], clickListener)
    }

    fun addMovie(movie: List<Movie.Results>) {
        movies.clear()
        movies.addAll(movie)

    }

    fun addMovieMore(movie: List<Movie.Results>) {
        movies.addAll(movie)

    }

}