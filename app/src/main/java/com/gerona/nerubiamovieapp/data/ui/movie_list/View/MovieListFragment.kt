package com.gerona.nerubiamovieapp.data.ui.movie_list.View

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.gerona.nerubiamovieapp.R
import com.gerona.nerubiamovieapp.data.api.ApiHelper
import com.gerona.nerubiamovieapp.data.api.RetrofitBuilder
import com.gerona.nerubiamovieapp.data.model.Movie
import com.gerona.nerubiamovieapp.data.ui.base.ViewModelFactory
import com.gerona.nerubiamovieapp.data.ui.movie_list.scroll.OnLoadMoreListener
import com.gerona.nerubiamovieapp.data.ui.movie_list.scroll.RecyclerViewLoadMoreScroll
import com.gerona.nerubiamovieapp.data.ui.movie_list.adapter.MovieAdapter
import com.gerona.nerubiamovieapp.data.ui.movie_list.viewmodel.ListViewModel
import com.gerona.nerubiamovieapp.databinding.FragmentMovieListBinding
import com.gerona.nerubiamovieapp.utils.SharedPref
import com.gerona.nerubiamovieapp.utils.Status
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_movie_list.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MovieListFragment : Fragment() {
    private lateinit var binding: FragmentMovieListBinding
    lateinit var navController: NavController
    lateinit var scrollListener: RecyclerViewLoadMoreScroll
    var currentPage = 1;
    private lateinit var viewModel: ListViewModel
    private lateinit var adapter: MovieAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_movie_list, container, false
        )
        setupViewModel()
//        setupUI()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        setupUI()
        currentPage = SharedPref.read("page",1)!!
        setupObservers(currentPage)
    }

    private fun setupUI() {
        recyclerView.layoutManager = LinearLayoutManager(activity)
        adapter = MovieAdapter { selectedItem: Movie.Results -> listItemClicked(selectedItem) }
        recyclerView.adapter = adapter
        scrollListener =
            RecyclerViewLoadMoreScroll(
                recyclerView.layoutManager as LinearLayoutManager
            )
        scrollListener.setOnLoadMoreListener(object :
            OnLoadMoreListener {
            override fun onLoadMore() {
                LoadMoreData()
            }
        })
        recyclerView.addOnScrollListener(scrollListener)
    }

    private fun LoadMoreData() {
        Log.e("setupUI", "LoadMoreData")
        scrollListener.setLoaded()
        currentPage++;
        setupObservers(currentPage)
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(ApiHelper(RetrofitBuilder.apiService))
        ).get(ListViewModel::class.java)
        binding.movieViewModel = viewModel
        binding.lifecycleOwner = this

    }

    private fun setupObservers(page: Int) {
        progressBar.visibility = View.VISIBLE
        viewModel.getPopularMovies(page).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        resource.data?.let { movie -> addMoreMovies(movie) }
                        recyclerView.visibility = View.VISIBLE

                    }
                    Status.ERROR -> {
                        progressBar.visibility = View.GONE
                        Log.e("ERROR", it.message!!);
                        Toast.makeText(activity, it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                        progressBar.visibility = View.VISIBLE
                    }
                }
            }
        })
    }

    private fun retrieveList(movie: Movie) {
        Log.e("setupObservers", Gson().toJson(movie))
        adapter.apply {
            addMovie(movie.results)
            notifyDataSetChanged()
        }
    }

    private fun addMoreMovies(movie: Movie) {
        Log.e("setupObservers", Gson().toJson(movie))
        SharedPref.write("page",currentPage)
        GlobalScope.launch(context = Dispatchers.Main) {
            delay(500)
            scrollListener.setLoaded()
            progressBar.visibility = View.GONE
            adapter.apply {
                addMovieMore(movie.results)
                notifyDataSetChanged()
            }
        }
        //Update the recyclerView in the main thread

    }

    private fun listItemClicked(movie: Movie.Results) {
        Toast.makeText(activity, "selected name is ${movie.title}", Toast.LENGTH_LONG).show()
//        subscriberViewModel.initUpdateAndDelete(subscriberViewModeliber)
        val bundle = bundleOf(
            "movie" to movie
        )
        navController!!.navigate(R.id.action_movieListFragment_to_movieDetailFragment, bundle)
    }


}