package com.gerona.nerubiamovieapp.data.repository

import com.gerona.nerubiamovieapp.data.api.ApiHelper

class MainRepository(private val apiHelper: ApiHelper) {

    suspend fun getPopularMovies(page: Int) = apiHelper.getPopularMovies(page)
}