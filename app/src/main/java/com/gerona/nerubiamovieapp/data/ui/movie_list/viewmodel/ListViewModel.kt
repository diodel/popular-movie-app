package com.gerona.nerubiamovieapp.data.ui.movie_list.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.gerona.nerubiamovieapp.data.repository.MainRepository
import com.gerona.nerubiamovieapp.utils.Resource
import kotlinx.coroutines.Dispatchers

class ListViewModel (private val mainRepository: MainRepository) : ViewModel() {
    fun getPopularMovies(page: Int) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.getPopularMovies(page)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }
}