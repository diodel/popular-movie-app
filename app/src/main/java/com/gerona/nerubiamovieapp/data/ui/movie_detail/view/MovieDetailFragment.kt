package com.gerona.nerubiamovieapp.data.ui.movie_detail.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.gerona.nerubiamovieapp.R
import com.gerona.nerubiamovieapp.data.model.Movie
import com.gerona.nerubiamovieapp.databinding.FragmentMovieDetailBinding
import com.gerona.nerubiamovieapp.databinding.FragmentMovieListBinding
import kotlinx.android.synthetic.main.fragment_movie_detail.*
import java.text.SimpleDateFormat
import java.util.*

class MovieDetailFragment : Fragment() {
    lateinit var movie: Movie.Results
    private lateinit var binding: FragmentMovieDetailBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        movie = arguments!!.getParcelable<Movie.Results>("movie")!!
        Log.e("movie", movie.title)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_movie_detail, container, false
        )
        binding.mov = movie
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide.with(binding.ivPoster.context)
            .load("https://image.tmdb.org/t/p/w500" + movie.poster_path)
            .placeholder(ColorDrawable(Color.GRAY))
            .into(binding.ivPoster)
//        tv_title.text = movie.title
        tv_date.text = parseDate(movie.release_date)

    }

    private fun parseDate(date: Date): String? {

        val dateFormat = SimpleDateFormat("dd MMM yyyy")

        return dateFormat.format(date)
    }
}