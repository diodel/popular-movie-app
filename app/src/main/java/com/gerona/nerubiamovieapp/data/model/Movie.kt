package com.gerona.nerubiamovieapp.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*
@Parcelize
data class Movie(
    val page: Int,
    val total_results: Int,
    val total_pages: Int,
    val results: List<Results>
): Parcelable{
    @Parcelize
    data class Results(
        val popularity: Double,
        val vote_count: Int,
        val video: Boolean,
        val poster_path: String,
        val id: Int,
        val title: String,
        val vote_average: Float,
        val overview: String,
        val release_date: Date

    ): Parcelable
}
