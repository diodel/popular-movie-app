package com.gerona.nerubiamovieapp.data.ui.movie_list.View

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.gerona.nerubiamovieapp.R
import com.gerona.nerubiamovieapp.utils.SharedPref

class MovieListActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_list)
        SharedPref.init(this)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.title = title
    }

    override fun onDestroy() {
        super.onDestroy()
        SharedPref.clearAll()

    }


}