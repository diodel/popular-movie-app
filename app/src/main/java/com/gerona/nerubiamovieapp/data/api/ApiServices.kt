package com.gerona.nerubiamovieapp.data.api

import com.gerona.nerubiamovieapp.data.model.Movie
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiServices {

    @GET("/3/movie/popular?api_key=26352a787bdf54e414e500ecf0af412f&language=en-US")
    suspend fun getAllPopularMovies (@Query("page") page: Int) : Movie
}