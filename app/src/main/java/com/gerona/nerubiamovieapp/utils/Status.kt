package com.gerona.nerubiamovieapp.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}